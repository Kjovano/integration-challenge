# integration-challenge 
-----------------------
Hello, welcome to the implementation of Kleber Jovano technical test.
I particularly liked the IDE Anypoint Studio, it was a few times that I felt excited about an integration tool / SOA.
I believe I have been able to resolve all of the proposed scenarios.
-----------------------
### Pre-requisites:
* Postman
* soapUI

Clone this repository:
```sh
git clone https://Kjovano@bitbucket.org/Kjovano/integration-challenge.git
```
-----------------------
### This solution consists of two services, a service of inclusion and consultation of employees and another of bonus salary and average salary.

+ For consultation and inclusion of employees a rest service was made available, below is the URI and an example in json of how to include employees.
* GET http://localhost:8081/employee
* POST http://localhost:8081/employee 		               			
```
#!json
[
  {
    "LAST_NAME": "Hamilton",
    "FIRST_NAME": "Lewis",
    "SALARY": 400000
  },
  {
    "LAST_NAME": "Vettel",
    "FIRST_NAME": "Sebastian",
    "SALARY": 300000
  },
  {
    "LAST_NAME": "Verstappen",
    "FIRST_NAME": "Max",
    "SALARY": 200000
  },
  {
    "LAST_NAME": "Massa",
    "FIRST_NAME": "Felipe",
    "SALARY": 100000
  }
]

``` 
+ For consultation of salary and bonus was implemented the service below with two operations:
* getAverageSalary
```
#!xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:emp="http://www.avenuecode.com/employee/">
   <soapenv:Header/>
   <soapenv:Body>
      <emp:getAverageSalary/>
   </soapenv:Body>
</soapenv:Envelope>

```
* getBonus 
```
#!xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:emp="http://www.avenuecode.com/employee/">
   <soapenv:Header/>
   <soapenv:Body>
      <emp:getBonus>
         <employeeId>3</employeeId>
      </emp:getBonus>
   </soapenv:Body>
</soapenv:Envelope>

```


-----------------------
### References:
* https://docs.mulesoft.com/


Thank You