%dw 1.0
%output application/java
---
{
	acceptNull: true,
	allowNullReturn: true,
	annotations: {
	},
	encoding: "????",
	endpoint: {
		address: "????",
		connector: {
			connected: true,
			defaultExchangePattern: "????",
			dispatcherFactory: {
			} as :object {
				class : "org.mule.api.transport.MessageDispatcherFactory"
			},
			disposed: true,
			inboundExchangePatterns: ["????"],
			muleContext: {
			} as :object {
				class : "org.mule.api.MuleContext"
			},
			outboundExchangePatterns: ["????"],
			protocol: "????",
			requesterFactory: {
			} as :object {
				class : "org.mule.api.transport.MessageRequesterFactory"
			},
			responseEnabled: true,
			retryPolicyTemplate: {
			} as :object {
				class : "org.mule.api.retry.RetryPolicyTemplate"
			},
			sessionHandler: {
			} as :object {
				class : "org.mule.api.transport.SessionHandler"
			},
			started: true,
			name: "????",
			connected: true,
			connectionDescription: "????",
			lifecycleState: {
			} as :object {
				class : "org.mule.api.lifecycle.LifecycleState"
			}
		} as :object {
			class : "org.mule.api.transport.Connector"
		},
		deleteUnacceptedMessages: true,
		disableTransportTransformer: true,
		encoding: "????",
		endpointBuilderName: "????",
		endpointURI: {
			address: "????",
			authority: "????",
			connectorName: "????",
			endpointName: "????",
			filterAddress: "????",
			fullScheme: "????",
			host: "????",
			muleContext: {
			} as :object {
				class : "org.mule.api.MuleContext"
			},
			params: {
			},
			password: "????",
			path: "????",
			port: 1,
			query: "????",
			resourceInfo: "????",
			responseTransformers: "????",
			scheme: "????",
			schemeMetaInfo: "????",
			transformers: "????",
			uri: {
			} as :object {
				class : "java.net.URI"
			},
			user: "????",
			userInfo: "????",
			userParams: {
			}
		} as :object {
			class : "org.mule.api.endpoint.EndpointURI"
		},
		exchangePattern: "????",
		filter: {
		} as :object {
			class : "org.mule.api.routing.filter.Filter"
		},
		initialState: "????",
		messageProcessors: [{
		} as :object {
			class : "org.mule.api.processor.MessageProcessor"
		}],
		messageProcessorsFactory: {
		} as :object {
			class : "org.mule.api.endpoint.EndpointMessageProcessorChainFactory"
		},
		mimeType: "????",
		muleContext: {
			artifactType: "????",
			client: {
			} as :object {
				class : "org.mule.api.client.LocalMuleClient"
			},
			clusterId: "????",
			clusterNodeId: 1,
			configuration: {
			} as :object {
				class : "org.mule.api.config.MuleConfiguration"
			},
			configurationAnnotations: {
			},
			dataTypeConverterResolver: {
			} as :object {
				class : "org.mule.DataTypeConversionResolver"
			},
			defaultExceptionStrategy: {
			} as :object {
				class : "org.mule.api.exception.MessagingExceptionHandler"
			},
			defaultMessageDispatcherThreadingProfile: {
			} as :object {
				class : "org.mule.api.config.ThreadingProfile"
			},
			defaultMessageReceiverThreadingProfile: {
			} as :object {
				class : "org.mule.api.config.ThreadingProfile"
			},
			defaultMessageRequesterThreadingProfile: {
			} as :object {
				class : "org.mule.api.config.ThreadingProfile"
			},
			defaultServiceThreadingProfile: {
			} as :object {
				class : "org.mule.api.config.ThreadingProfile"
			},
			defaultThreadingProfile: {
			} as :object {
				class : "org.mule.api.config.ThreadingProfile"
			},
			disposed: true,
			disposing: true,
			endpointFactory: {
			} as :object {
				class : "org.mule.api.endpoint.EndpointFactory"
			},
			exceptionContextProviders: [{
			} as :object {
				class : "org.mule.api.execution.ExceptionContextProvider"
			}],
			exceptionListener: {
			} as :object {
				class : "org.mule.api.exception.SystemExceptionHandler"
			},
			executionClassLoader: {
			} as :object {
				class : "java.lang.ClassLoader"
			},
			expressionLanguage: {
			} as :object {
				class : "org.mule.api.el.ExpressionLanguage"
			},
			expressionManager: {
			} as :object {
				class : "org.mule.api.expression.ExpressionManager"
			},
			extensionManager: {
			} as :object {
				class : "org.mule.extension.ExtensionManager"
			},
			flowTraceManager: {
			} as :object {
				class : "org.mule.api.context.notification.FlowTraceManager"
			},
			initialised: true,
			initialising: true,
			injector: {
			} as :object {
				class : "org.mule.api.Injector"
			},
			lifecycleManager: {
			} as :object {
				class : "org.mule.api.lifecycle.LifecycleManager"
			},
			lockFactory: {
			} as :object {
				class : "org.mule.util.lock.LockFactory"
			},
			notificationManager: {
			} as :object {
				class : "org.mule.context.notification.ServerNotificationManager"
			},
			objectSerializer: {
			} as :object {
				class : "org.mule.api.serialization.ObjectSerializer"
			},
			objectStoreManager: {
			} as :object {
				class : "org.mule.api.store.ObjectStoreManager"
			},
			primaryPollingInstance: true,
			processorTimeWatcher: {
			} as :object {
				class : "org.mule.management.stats.ProcessingTimeWatcher"
			},
			queueManager: {
			} as :object {
				class : "org.mule.util.queue.QueueManager"
			},
			registry: {
			} as :object {
				class : "org.mule.api.registry.MuleRegistry"
			},
			securityManager: {
			} as :object {
				class : "org.mule.api.security.SecurityManager"
			},
			startDate: 1,
			started: true,
			starting: true,
			statistics: {
			} as :object {
				class : "org.mule.management.stats.AllStatistics"
			},
			stopped: true,
			stopping: true,
			streamCloserService: {
			} as :object {
				class : "org.mule.api.util.StreamCloserService"
			},
			transactionFactoryManager: {
			} as :object {
				class : "org.mule.api.SingleResourceTransactionFactoryManager"
			},
			transactionManager: {
			} as :object {
				class : "javax.transaction.TransactionManager"
			},
			uniqueIdString: "????",
			workListener: {
			} as :object {
				class : "javax.resource.spi.work.WorkListener"
			},
			workManager: {
			} as :object {
				class : "org.mule.api.context.WorkManager"
			}
		} as :object {
			class : "org.mule.api.MuleContext"
		},
		properties: {
		},
		protocol: "????",
		readOnly: true,
		redeliveryPolicy: {
			annotations: {
			},
			listener: {
			} as :object {
				class : "org.mule.api.processor.MessageProcessor"
			},
			maxRedeliveryCount: 1,
			muleContext: {
			} as :object {
				class : "org.mule.api.MuleContext"
			},
			theFailedMessageProcessor: {
			} as :object {
				class : "org.mule.api.processor.MessageProcessor"
			}
		} as :object {
			class : "org.mule.processor.AbstractRedeliveryPolicy"
		},
		responseMessageProcessors: [{
		} as :object {
			class : "org.mule.api.processor.MessageProcessor"
		}],
		responseTimeout: 1,
		responseTransformers: [{
		} as :object {
			class : "org.mule.api.transformer.Transformer"
		}],
		retryPolicyTemplate: {
			metaInfo: {
			},
			notifier: {
			} as :object {
				class : "org.mule.api.retry.RetryNotifier"
			}
		} as :object {
			class : "org.mule.api.retry.RetryPolicyTemplate"
		},
		securityFilter: {
			credentialsAccessor: {
			} as :object {
				class : "org.mule.api.security.CredentialsAccessor"
			},
			securityManager: {
			} as :object {
				class : "org.mule.api.security.SecurityManager"
			},
			securityProviders: "????"
		} as :object {
			class : "org.mule.api.security.EndpointSecurityFilter"
		},
		transactionConfig: {
			action: "????",
			configured: true,
			constraint: {
			} as :object {
				class : "org.mule.transaction.constraints.ConstraintFilter"
			},
			factory: {
			} as :object {
				class : "org.mule.api.transaction.TransactionFactory"
			},
			interactWithExternal: true,
			timeout: 1,
			transacted: true
		} as :object {
			class : "org.mule.api.transaction.TransactionConfig"
		},
		transformers: [{
		} as :object {
			class : "org.mule.api.transformer.Transformer"
		}],
		name: "????"
	} as :object {
		class : "org.mule.api.endpoint.ImmutableEndpoint"
	},
	ignoreBadInput: true,
	mimeType: "????",
	name: "????",
	returnClass: "????",
	returnDataType: {
		encoding: "????",
		mimeType: "????",
		type: "????"
	} as :object {
		class : "org.mule.api.transformer.DataType"
	},
	sourceDataTypes: [{
		encoding: "????",
		mimeType: "????",
		type: "????"
	} as :object {
		class : "org.mule.api.transformer.DataType"
	}],
	sourceTypes: ["????"]
} as :object {
	class : "com.avenuecode.challenge.integration.EmployeeBonusTransformer"
}