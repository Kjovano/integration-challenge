package com.avenuecode.challenge.integration;

import java.math.BigDecimal;

import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;

public class EmployeeAverageTransformer extends AbstractTransformer {

	@Override
	protected Object doTransform(Object payload, String enc) throws TransformerException {

		String s = "";
		BigDecimal result = null;

		try {

			if (payload != null) {

				s = payload.toString();
				s = s.substring(s.indexOf("<AVERAGE>") + 9, s.lastIndexOf("</AVERAGE>"));
				result = new BigDecimal(s);
			}

		} catch (Exception ex) {
			return ex;
		}

		return result.setScale(2, BigDecimal.ROUND_HALF_UP);

	}

}
