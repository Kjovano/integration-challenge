package com.avenuecode.challenge.integration;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.Random;

import org.jetel.data.primitive.Decimal;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.pubsub.PayloadItem;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;

import com.google.protobuf.GeneratedMessage;

public class EmployeeBonusTransformer extends AbstractTransformer {

	@Override
	protected Object doTransform(Object payload, String enc) throws TransformerException {

		BigDecimal bonus = null;
		String s = "";		
		BigDecimal result = null;		
		BigDecimal valor = null;		
		BigDecimal min = new BigDecimal(2.00); 
		BigDecimal max = new BigDecimal(0.00);

		// TODO Insert the bonus implementation here!		

		try {

			if (payload != null) {
				
				s = payload.toString();
				s = s.substring(s.indexOf("<BONUS>") + 7, s.lastIndexOf("</BONUS>"));
				valor = new BigDecimal(s);				

				//
				valor = valor.multiply(new BigDecimal(50).divide(new BigDecimal(100))) ;
				
				max = valor; 
				
				BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
				
				result = randomBigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);								

				//
				bonus = result;
			}

		} catch (Exception ex) {
			return ex;
		}

		return bonus;
	}

}
